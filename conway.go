package main

import (
	"model"
	"util"
)

func GameOfLive(rule util.Rule, currentGenereation model.Game) model.Game {
	lines, columns := model.GetLinesAndColumnsFromGame(currentGenereation)

	nextGeneration := model.NewGame(columns, lines)

	for x := 0; x < columns; x++ {
		for y := 0; y < lines; y++ {
			cell, age := applyRules(rule, currentGenereation, x, y)
			nextGeneration.Field[y][x] = cell
			nextGeneration.Age[y][x] = age
		}
	}

	return nextGeneration
}

func applyRules(rule util.Rule, game model.Game, x, y int) (bool, int) {
	neighbours := countNeighbours(game.Field, x, y)
	newAge := 0

	if game.Field[y][x] {
		isSurvived := util.Contains(rule.Survive, neighbours)
		if isSurvived {
			newAge = game.Age[y][x] + 1
		}
		return isSurvived, newAge
	}
	isBorn := util.Contains(rule.Born, neighbours)
	return isBorn, newAge
}

func countNeighbours(field [][]bool, x, y int) int {
	lines, columns := model.GetLinesAndColumnsFromField(field)

	// mathematical correct modulo/remiander important for negativ numbers
	yTop := (((y + 1) % lines) + lines) % lines
	yBot := (((y - 1) % lines) + lines) % lines
	xLeft := (((x + 1) % columns) + columns) % columns
	xRight := (((x - 1) % columns) + columns) % columns

	neighbours := []bool{field[yBot][xLeft], field[yBot][x], field[yBot][xRight],
		field[y][xLeft], field[y][xRight],
		field[yTop][xLeft], field[yTop][x], field[yTop][xRight]}

	var n int

	for _, b := range neighbours {
		if b {
			n++
		}
	}

	return n
}
