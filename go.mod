module cgolgo

go 1.17

require (
	github.com/leaanthony/clir v1.0.4
	model v0.0.0-00010101000000-000000000000
	util v0.0.0-00010101000000-000000000000
)

replace util => ./util

replace model => ./model
