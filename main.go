package main

import (
	"log"
	"math/rand"
	"strconv"
	"time"

	"github.com/leaanthony/clir"

	"model"
	"util"
)

func main() {
	rand.Seed(time.Now().UnixNano())

	cli := clir.NewCli("Conways game of Life", "Cellular automata", "v1.0.0")

	// sleep = 7 ~ 144hz
	// sleep = 8 ~ 120hz
	// sleep = 16 ~ 60hz
	columns, lines, iterations, sleep := 318, 77, 10000, 8
	pattern := ""

	cli.IntFlag("c", "Columns (x axis)", &columns)
	cli.IntFlag("l", "Rows (y axis)", &lines)
	cli.IntFlag("i", "Amount of conway iterations", &iterations)
	cli.IntFlag("s", "Sleep in ms (time between to renderings)", &sleep)
	cli.StringFlag("p", "Start conway with a specific pattern", &pattern)

	sleepDuration, _ := time.ParseDuration(strconv.Itoa(sleep) + "ms")

	cli.Action(func() error {
		g := model.NewGame(columns, lines)

		if len(pattern) != 0 {
			util.Parse(pattern, &g)
		} else {
			model.Initialize(&g)
		}

		r := util.New("b3/s23")

		for i := 0; i < iterations; i++ {
			util.Print(g, i)
			time.Sleep(sleepDuration)
			g = GameOfLive(r, g)
		}
		return nil
	})
	err := cli.Run()
	if err != nil {
		log.Fatal(err)
	}
}
