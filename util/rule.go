package util

import (
	"strconv"
	"strings"
)

type Rule struct {
	Born    []int
	Survive []int
}

func New(ruleString string) Rule {
	ruleString = strings.ToLower(ruleString)

	rule := Rule{}

	// validation
	if strings.Contains(ruleString, "/") {
		x := strings.Split(ruleString, "/")
		if strings.HasPrefix(x[0], "b") {
			b := x[0][1:]
			for _, c := range b {
				n, _ := strconv.Atoi(string(c))
				rule.Born = append(rule.Born, n)
			}
		}
		if strings.HasPrefix(x[1], "s") {
			s := x[1][1:]
			for _, c := range s {
				n, _ := strconv.Atoi(string(c))
				rule.Survive = append(rule.Survive, n)
			}
		}

	}

	return rule
}

func Contains(slice []int, e int) bool {
	for _, a := range slice {
		if a == e {
			return true
		}
	}
	return false
}
