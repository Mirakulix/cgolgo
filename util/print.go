package util

import (
	"fmt"
	"model"
	"strconv"
	"strings"
)

func Print(game model.Game, iteration int) {
	s := strings.Builder{}

	lines, columns := model.GetLinesAndColumnsFromGame(game)

	for y := 0; y < lines; y++ {
		for x := 0; x < columns; x++ {
			if game.Field[y][x] {
				s.WriteString(colourMap(game.Age[y][x]))
				s.WriteString("█")
				s.WriteString("\u001B[0m")
			} else {
				s.WriteString(" ")
			}
		}
		s.WriteString("\n")
	}
	s.WriteString(strings.Repeat("_", columns))
	s.WriteString("Iteration: " + strconv.Itoa(iteration))
	s.WriteString(" Length: " + strconv.Itoa(columns))
	s.WriteString(" Width: " + strconv.Itoa(lines) + "\n")
	fmt.Print(s.String())
	s.Reset()
}

func colourMap(age int) string {
	switch age {
	case 0:
		return "\u001B[38;5;34m"
	case 1:
		return "\u001B[38;5;35m"
	case 2:
		return "\u001B[38;5;36m"
	case 3:
		return "\u001B[38;5;37m"
	case 4:
		return "\u001B[38;5;38m"
	case 5:
		return "\u001B[38;5;39m"
	case 6:
		return "\u001B[38;5;40m"
	case 7:
		return "\u001B[38;5;41m"
	case 8:
		return "\u001B[38;5;42m"
	case 9:
		return "\u001B[38;5;43m"
	case 10:
		return "\u001B[38;5;44m"
	case 11:
		return "\u001B[38;5;45m"
	case 12:
		return "\u001B[38;5;46m"
	case 13:
		return "\u001B[38;5;47m"
	case 14:
		return "\u001B[38;5;48m"
	case 15:
		return "\u001B[38;5;49m"
	default:
		return "\u001B[38;5;21m"
	}
}
