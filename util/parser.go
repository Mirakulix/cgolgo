package util

import (
	"bufio"
	"model"
	"os"
	"strconv"
	"strings"
)

const cellsSuffix = ".cells"
const rleSuffix = ".rle"
const CELLS_DEAD = "."
const CELLS_ALIVE = "O"
const RLE_DEAD = "b"
const RLE_ALIVE = "o"
const RLE_END_OF_FILE = "!"
const RLE_NEW_LINE = "$"
const COMMENT = "!"
const COMMENT2 = "#"
const RULE_LINE = "x"
const RULE_LINE2 = "y"
const RULE_LINE3 = "rule"

func Parse(filepath string, game *model.Game) {
	lines := readAllLines(filepath)

	if strings.HasSuffix(filepath, cellsSuffix) {
		parseCells(lines, game)
	} else {
		lines = rleToCells(lines)
		parseCells(lines, game)
	}
}

func readAllLines(filepath string) []string {
	f, err := os.Open(filepath)
	if err != nil {
		return nil
	}
	defer f.Close()

	var lines []string
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		s := scanner.Text()
		if !startsWithComment(s) {
			lines = append(lines, s)
		}
	}

	return lines
}

func startsWithComment(s string) bool {
	if strings.HasPrefix(s, COMMENT) || strings.HasPrefix(s, COMMENT2) || strings.HasPrefix(s, RULE_LINE) || strings.HasPrefix(s, RULE_LINE2) || strings.HasPrefix(s, RULE_LINE3) {
		return true
	}
	return false
}

func parseCells(cellsFile []string, game *model.Game) {
	x, y := 0, 0

	for _, line := range cellsFile {
		for i := 0; i < len(line); i++ {
			if string(line[i]) == CELLS_ALIVE {
				game.Field[y][x] = true
				game.Age[y][x] = 0
			}
			x++
		}
		x = 0
		y++
	}
}

func rleToCells(rleFile []string) []string {
	repeater := strings.Builder{}
	var cellsFile []string
	line := strings.Builder{}

	var rleString string
	for _, s := range rleFile {
		rleString += s
	}

	for i := 0; i < len(rleString); i++ {
		switch cell := string(rleString[i]); cell {
		case RLE_DEAD:
			appendToLine(&repeater, &line, CELLS_DEAD)
		case RLE_ALIVE:
			appendToLine(&repeater, &line, CELLS_ALIVE)
		case RLE_END_OF_FILE:
			cellsFile = append(cellsFile, line.String())
		case RLE_NEW_LINE:
			cellsFile = append(cellsFile, line.String())
			line.Reset()
		default:
			repeater.WriteString(string(rleString[i]))
		}

	}

	return cellsFile
}

func appendToLine(repeater, line *strings.Builder, aliveCell string) {
	if repeater.Len() == 0 {
		line.WriteString(aliveCell)
	} else {
		count, _ := strconv.Atoi(repeater.String())
		line.WriteString(strings.Repeat(aliveCell, count))
		repeater.Reset()
	}
}
