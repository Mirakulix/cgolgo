package model

import (
	"math/rand"
)

type Game struct {
	Field [][]bool
	Age   [][]int
}

func NewGame(x, y int) Game {
	f := make([][]bool, y)
	for i := range f {
		f[i] = make([]bool, x)
	}
	a := make([][]int, y)
	for i := range f {
		a[i] = make([]int, x)
	}
	return Game{f, a}
}

func Initialize(game *Game) {
	lines, columns := GetLinesAndColumnsFromGame(*game)

	for x := 0; x < columns; x++ {
		for y := 0; y < lines; y++ {
			game.Field[y][x] = randBool()
		}
	}
}

func GetLinesAndColumnsFromGame(game Game) (int, int) {
	lines := len(game.Field)
	columns := len(game.Field[0])

	return lines, columns
}

func GetLinesAndColumnsFromField(field [][]bool) (int, int) {
	lines := len(field)
	columns := len(field[0])

	return lines, columns
}

func randBool() bool {
	return rand.Intn(2) == 0
}
